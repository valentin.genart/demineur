package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import main.Grille;

/**
 * Classe de test sur la grille.
 * @author Valentin
 *
 */
class testInitGrille {

	/**
	 * Teste si la première case sur laquelle le joueur clique renvoie toujours 0 bombe autour.
	 */
	@Test
	void testFirstCaseZero() {
		Grille g = Grille.getInstance();
		g.setTailleGrille(3);
		g.generateGrille(1, 1);
		assertEquals(g.getGrille()[1][1].getNbBombesAutour(), 0);
	}
}
