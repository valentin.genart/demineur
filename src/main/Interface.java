package main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.SortedMap;
import java.util.Stack;
import java.util.TreeMap;

import javax.swing.JOptionPane;
import javax.swing.Timer;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * L'interface graphique du jeu.
 * @author Valentin
 *
 */
public class Interface extends Application{

	/**
	 * Méthode principale lançant le programme.
	 * @param args les arguments du programme.
	 */
	public static void main(String[] args) {
		Application.launch(args);
	}

	/**
	 * String contenant la taille d'une dimension de la grille saisie par le joueur.
	 */
	private String dim;

	/**
	 * Le fichier CSV contenant les scores.
	 */
	private File fichierScores;

	/**
	 * Booléen true si le clic du joueur est le premier de la partie.
	 */
	private boolean firstClick = true;

	/**
	 * Booléen true si le joueur a gagné.
	 */
	private boolean gagne;

	/**
	 * Le contexte graphique permettant de dessiner la grille et d'afficher les nombres etc...
	 */
	private GraphicsContext gc;

	/**
	 * La grille de jeu.
	 */
	private Grille grille;

	/**
	 * la taille d'une dimension de la grille.
	 */
	private int intDim;

	/**
	 * Le nombre de bombes non encore découvertes par le joueur.
	 */
	private int nbBombesRestantes;

	/**
	 * Booléen true si le joueur a perdu.
	 */
	private boolean perdu;

	/**
	 * Stack permettant d'empiler les cases autour d'une case 0 afin de toutes les dévoiler.
	 */
	private Stack<Case> pileCases = new Stack<>();

	/**
	 * Le nombre de secondes écoulées depuis le début d'une partie.
	 */
	private int secondesEcoulees;

	/**
	 * Le texte affichant le nombre de bombes restantes.
	 */
	private Text textNbBombes;

	/**
	 * Le texte affichant les meilleurs scores pour la taille de la grille choisie.
	 */
	private Text textScores;

	/**
	 * Le texte affichant le temps écoulé depuis le début de la partie.
	 */
	private Text textTimer;

	/**
	 * Le champ de texte permettant au joueur de renseigner son nom.
	 */
	private TextField tfNom;

	/**
	 * Timer permettant de compter le temps écoulé depuis le début d'une partie.
	 */
	private Timer timer;

	/**
	 * Action effectuée lors du clic sur une case.
	 * @param intDim la taille d'une dimension de la grille.
	 * @param e le clic
	 */
	private void actionClick(int intDim, MouseEvent e) {
		//On vérifie que le clic est dans la grille.
		if(e.getX()>50 && e.getX()<550 && e.getY()>50 && e.getY()<550 && !perdu && !gagne) {
			int dimX = 0, dimY = 0;
			//On cherche l'index i et j de la case en fonction des coordonnées du clic.
			for(int i=0; i<intDim; i++) {
				if(e.getX()>=50+i*(500/intDim) && e.getX() <50+(i+1)*500/intDim)
					dimY=i;
				if(e.getY()>=50+i*(500/intDim) && e.getY() <50+(i+1)*500/intDim)
					dimX=i;
			}
			if(firstClick && e.getButton().equals(MouseButton.PRIMARY))
				doAtFirstClick(dimX, dimY);
			if(e.getButton().equals(MouseButton.PRIMARY) && !grille.getGrille()[dimX][dimY].isMarked())
				actionLeftClick(dimX, dimY);
			else if(e.getButton().equals(MouseButton.SECONDARY) && !firstClick)
				actionRightClick(dimX, dimY);
		}
	}

	/**
	 * Action effectuée lors du clic gauche (révéler la case).
	 * @param dimX l'index i de la case sur lequel le joueur a cliqué dans la grille.
	 * @param dimY l'index j de la case sur lequel le joueur a cliqué dans la grille.
	 */
	private void actionLeftClick(int dimX, int dimY) {
		if(grille.getGrille()[dimX][dimY].isBomb())
			perdu(dimX, dimY);
		else {
			reveler(dimX, dimY);
			if(finPartie()) {
				timer.stop();
				ajouterScore();
				JOptionPane.showMessageDialog(null, "Bravo ! Vous avez gagné !", "Bravo !", JOptionPane.INFORMATION_MESSAGE);	
			}
		}
	}

	/**
	 * Action effectuée lors du clic droit (dépôt ou retrait d'un flag).
	 * @param dimX l'index i de la case sur lequel le joueur a cliqué dans la grille.
	 * @param dimY l'index j de la case sur lequel le joueur a cliqué dans la grille.
	 */
	private void actionRightClick(int dimX, int dimY) {
		int milieuCaseX = grille.getGrille()[dimX][dimY].getMilieuX();
		int milieuCaseY = grille.getGrille()[dimX][dimY].getMilieuY();
		//Si la case est marquée par un flag, on enlève le flag et on incrémente le nombre de bombes restantes.
		if(grille.getGrille()[dimX][dimY].isMarked()) {
			gc.clearRect(milieuCaseX-10, milieuCaseY-15, 20, 25);
			grille.getGrille()[dimX][dimY].setMarked(false);
			nbBombesRestantes++;
			textNbBombes.setFill(Color.BLACK);
			textNbBombes.setText("Nombre de bombes restantes: "+ nbBombesRestantes);
		}
		/*Sinon si la case n'est pas marquée par un flag et qu'elle n'a pas été révélée, on place un flag et on décrémente 
		le nombre de bombes restantes.*/
		else if(!grille.getGrille()[dimX][dimY].isMarked() && grille.getGrille()[dimX][dimY].isHidden()){
			if(nbBombesRestantes>0) {
				gc.drawImage(new Image(new File("ressources/flag.png").toURI().toString()), milieuCaseX-10, milieuCaseY-10, 20, 20);
				grille.getGrille()[dimX][dimY].setMarked(true);
				nbBombesRestantes--;
				textNbBombes.setFill(Color.BLACK);
				textNbBombes.setText("Nombre de bombes restantes: "+ nbBombesRestantes);
			}
		}
	}

	/**
	 * Affiche les 5 meilleurs scores pour cette taille de grille.
	 */
	public void afficherScores(){
		textScores.setText("Scores :\n");
		fichierScores = new File("ressources/scores"+dim+".csv");
		try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader(fichierScores));
			String ligne;
			int i = 0;
			while((ligne = bufferedReader.readLine()) != null && i<5) {
				String[] elementsLigne = ligne.split(";");
				textScores.setText(textScores.getText()+ ++i + ". " + elementsLigne[0] + "\t" + elementsLigne[1] + "\n");
			}
			bufferedReader.close();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, new MessageErreur().getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Ajoute le score du joueur au fichier de scores s'il a gagné.
	 */
	public void ajouterScore() {
		String time = textTimer.getText().substring(textTimer.getText().length()-5);
		try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader(fichierScores));
			String ligne;
			SortedMap<Double, String> meilleursScores = new TreeMap<Double, String>();
			while((ligne = bufferedReader.readLine()) != null) {
				String[] elementsLigne = ligne.split(";");
				meilleursScores.put(convertirTempsEnSecondes(elementsLigne[1]), elementsLigne[0]);
			}
			String nom = tfNom.getText();
			if(nom.isEmpty() || onlySpaces(nom))
				nom = "Default";
			meilleursScores.put(convertirTempsEnSecondes(time), nom);
			PrintStream printStream = new PrintStream(new FileOutputStream(fichierScores));
			Set<Entry<Double, String>> set = meilleursScores.entrySet();
			for (Entry<Double, String> entry : set) {
				printStream.print(entry.getValue()+";");
				Integer minutes;
				Integer secondes;
				minutes = (int) (Math.round(entry.getKey())/60);
				secondes = (int) (Math.round(entry.getKey())-minutes*60);
				DecimalFormat formater = new DecimalFormat("00");
				printStream.print(formater.format(minutes)+":"+formater.format(secondes)+"\n");
			}
			bufferedReader.close();
			printStream.close();
		} catch (MessageErreur | NumberFormatException | IOException e) {
			JOptionPane.showMessageDialog(null, new MessageErreur().getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
		}
	}

	/**
	 * Permet au joueur de choisir la taille d'une dimension de la grille.
	 */
	private void choixDimension() {
		dim="";
		do {
			dim = JOptionPane.showInputDialog(null, "Saisissez la taille de la grille entre 5 et 15", "Taille de la grille", JOptionPane.QUESTION_MESSAGE);
			if(dim==null || !dim.matches("[0-9]+"))
				dim="0";
		}while(Integer.parseInt(dim) < 5 || Integer.parseInt(dim) > 15);
		intDim = Integer.parseInt(dim);
	}

	/**
	 * Convertit une chaîne de caractères représentant un temps sous la forme 00:00 en un double représentant le nombre total de secondes.
	 * @param timeString le temps sous la forme 00:00
	 * @return le temps en secondes
	 * @throws MessageErreur message affiché lorsqu'une erreur se produit.
	 */
	private double convertirTempsEnSecondes(String timeString) throws MessageErreur {
		if(timeString.matches("[0-9]{2}:[0-9]{2}")) {
			int minutes = Integer.parseInt(timeString.substring(0, 2));
			double secondes = Integer.parseInt(timeString.substring(3, 5));
			Random r = new Random();
			secondes+=0.000000001*r.nextInt(500000000); //On ajoute une partie d�cimale al�atoire pour g�rer les �galit�s. Ainsi, les 2 valeurs seront dans le Set et on arrondira � l'affichage pour qu'il y ait bien �galit�.
			return secondes+60*minutes;
		}
		else
			throw new MessageErreur();
	}

	/**
	 * Définit la couleur du texte du GraphicsContext, pour afficher les nombres de différentes couleurs.
	 * @param nbBombesAutour le nombre de bombes autour de la case à afficher.
	 */
	private void couleurGC(int nbBombesAutour) {
		switch (nbBombesAutour) {
		case 1:
			gc.setStroke(Color.BLUE);
			break;
		case 2:
			gc.setStroke(Color.GREEN);
			break;
		case 3:
			gc.setStroke(Color.RED);
			break;
		case 4:
			gc.setStroke(Color.DARKVIOLET);
			break;
		case 5:
			gc.setStroke(Color.BROWN);
			break;
		case 6:
			gc.setStroke(Color.DEEPPINK);
			break;
		case 7:
			gc.setStroke(Color.DARKORANGE);
			break;
		case 8:
			gc.setStroke(Color.CYAN);
			break;
		default:
			gc.setStroke(Color.BLACK);
			break;
		}	
	}

	/**
	 * Action effectuée lors du premier clic du joueur. On démarre le timer et génère la grille en fixant 0 bombe autour de la case
	 * sur laquelle le joueur a cliqué.
	 * @param dimX l'index i de la case sur lequel le joueur a cliqué dans la grille.
	 * @param dimY l'index j de la case sur lequel le joueur a cliqué dans la grille.
	 */
	private void doAtFirstClick(int dimX, int dimY) {
		secondesEcoulees = 0;
		grille.generateGrille(dimX, dimY);
		nbBombesRestantes = grille.getNbBombes();
		textNbBombes.setText("Nombre de bombes restantes: "+nbBombesRestantes);
		firstClick = false;
		timer.start();
		//grille.printGrille();
	}

	/**
	 * Renvoie true si le joueur a fini en gagnant la partie.
	 * @return true si le joueur a fini en gagnant la partie.
	 */
	private boolean finPartie() {
		gagne = true;
		for (Case c : grille) {
			if(!c.isBomb()) {
				if(c.isHidden())
					gagne = false;
			}
		}
		return gagne;
	}
	
	/**
	 * Vérifie que le nom n'est pas constitué uniquement d'espaces.
	 * @param nom le nom du joueur.
	 * @return true si le nom n'est composé que d'espaces.
	 */
	private boolean onlySpaces(String nom) {
		for(int i = 0; i < nom.length(); i++) {
			if(nom.charAt(i)!=' ')
				return false;
		}
		return true;
	}

	/**
	 * Méthode lancée lorsque le joueur a perdu, affichant toutes les bombes de la grille ainsi que les erreurs de flags et un message.
	 * @param dimX l'index i de la case sur lequel le joueur a cliqué dans la grille.
	 * @param dimY l'index j de la case sur lequel le joueur a cliqué dans la grille.
	 */
	private void perdu(int dimX, int dimY) {
		timer.stop();
		Image img = new Image(new File("ressources/bomb.png").toURI().toString());
		gc.drawImage(img, grille.getGrille()[dimX][dimY].getMilieuX()-10, grille.getGrille()[dimX][dimY].getMilieuY()-10, 20, 20);
		grille.getGrille()[dimX][dimY].setHidden(false);
		revelerBombes(img);
		JOptionPane.showMessageDialog(null, "Mince ! Vous avez perdu !", "Perdu !", JOptionPane.INFORMATION_MESSAGE);
		perdu = true;
	}

	/**
	 * Reset la partie avec les paramètres initiaux.
	 * @param intDim la taille d'une dimension de la grille.
	 */
	private void reset(int intDim) {
		//On redessine une grille vide.
		gc.clearRect(0, 0, 600, 600);
		gc.setStroke(Color.BLACK);
		gc.strokeRect(50, 50, 500, 500);
		grille.setTailleGrille(intDim);
		firstClick = true;
		perdu = gagne = false;
		timer.stop();
		secondesEcoulees = 0;
		afficherScores();
		textNbBombes.setText("Nombre de bombes restantes: ");
		textTimer.setText("Temps écoulé : 00:00");
		for(int i=0; i<intDim; i++) {
			gc.strokeLine(50, 50+i*(500/intDim), 550, 50+i*(500/intDim));
			gc.strokeLine(50+i*(500/intDim), 50, 50+i*(500/intDim), 550);
		}
	}

	/**
	 * Révèle la case sur laquelle le joueur a cliqué. Si la case a 0 bombe dans son voisinnage, toute la zone de case contenant des 
	 * 0 autour est révélée.
	 * @param dimX l'index i de la case sur lequel le joueur a cliqué dans la grille.
	 * @param dimY l'index j de la case sur lequel le joueur a cliqué dans la grille.
	 */
	private void reveler(int dimX, int dimY) {
		if(grille.getGrille()[dimX][dimY].isBomb())
			perdu(dimX, dimY);
		/*Si la case a 0 bombe autour, on place ses cases voisines dans une pile pour révéler chacune tour à tour récursivement.
		Si ces cases sont aussi des 0, on réitère la même opération dessus afin de découvrir toute la zone de 0.*/
		if(grille.getGrille()[dimX][dimY].getNbBombesAutour() == 0) {
			for(int i=dimX-1; i<=dimX+1; i++) {
				for(int j=dimY-1; j<=dimY+1; j++) {
					//On place la case dans la pile si elle ne l'a jamais été et qu'elle n'est pas marquée par un flag.
					if(!(i==dimX && j==dimY) && !perdu && i>=0 && j>=0 && i<grille.getTailleGrille() && j<grille.getGrille()[0].length && !grille.getGrille()[i][j].getPassageStack() && !grille.getGrille()[i][j].isMarked()) {
						pileCases.push(grille.getGrille()[i][j]);
						grille.getGrille()[i][j].setPassageStack(true);
					}
				}
			}
			//Tant que la pile n'est pas vide, on réitère l'opération sur la case en haut de la pile, qu'on enlève de la pile.
			while(!pileCases.isEmpty())
				reveler(pileCases.peek().getI(), pileCases.pop().getJ());
		}
		/*Si le joueur a cliqué sur une case déjà révélée, alors on révèle toutes les cases autour si le nombre de flags posés autour
		est égal ou supérieur au nombre de bombes autour de la case*/
		else if(!grille.getGrille()[dimX][dimY].isHidden())
			revelerCasesAutourSiAllFlags(dimX, dimY);
		//Si la case n'a pas été révélée et que le joueur n'a pas perdu, on la dévoile.
		if(!perdu && grille.getGrille()[dimX][dimY].isHidden()) {
			grille.getGrille()[dimX][dimY].setHidden(false);
			couleurGC(grille.getGrille()[dimX][dimY].getNbBombesAutour());
			gc.strokeText(""+grille.getGrille()[dimX][dimY].getNbBombesAutour(), grille.getGrille()[dimX][dimY].getMilieuX(), grille.getGrille()[dimX][dimY].getMilieuY());
		}
	}

	/**
	 * Révèle toutes les bombes de la grille lorsque le joueur a perdu et indique les flags mal placés.
	 * @param img l'image de bombe à afficher
	 */
	private void revelerBombes(Image img) {
		for (Case c : grille) {
			if(c.isBomb() && !c.isMarked() && c.isHidden())
				gc.drawImage(img, c.getMilieuX()-10, c.getMilieuY()-10, 20, 20);
			if(!c.isBomb() && c.isMarked())
				gc.drawImage(new Image(new File("ressources/flagerror.png").toURI().toString()), c.getMilieuX()-10, c.getMilieuY()-10, 20, 20);
		}
	}

	/**
	 * Révèle d'un seul coup toutes les cases sans flag autour d'une case déjà révélée sur laquelle le joueur a cliqué, si le nombre
	 * de flags posés autour de cette case est supérieur ou égal au nombre de bombes autour de cette case.
	 * @param dimX l'index i de la case autour de laquelle on veut révéler toutes les cases sans flag.
	 * @param dimY l'index j de la case autour de laquelle on veut révéler toutes les cases sans flag.
	 */
	private void revelerCasesAutourSiAllFlags(int dimX, int dimY) {
		//Comptage des flags autour de la case.
		int cptFlagsAutour = 0;
		for(int k=dimX-1; k<=dimX+1; k++) {
			for(int l=dimY-1; l<=dimY+1; l++) {
				if(k>=0 && k<grille.getTailleGrille() && l>=0 && l<grille.getTailleGrille() && grille.getGrille()[k][l].isMarked())
					cptFlagsAutour++;
			}
		}
		//Si le nombre de flags >= nombre de bombes, on révéle toutes les cases sans flags.
		if(cptFlagsAutour >= grille.getGrille()[dimX][dimY].getNbBombesAutour()) {
			for(int k=dimX-1; k<=dimX+1; k++) {
				for(int l=dimY-1; l<=dimY+1; l++) {
					if(!perdu && k>=0 && k<grille.getTailleGrille() && l>=0 && l<grille.getTailleGrille() && !grille.getGrille()[k][l].isMarked() && grille.getGrille()[k][l].isHidden())
						reveler(k, l);
				}
			}
		}
	}
	
	/**
	 * Méthode lançant l'interface.
	 */
	public void start(Stage stage) throws Exception {
		choixDimension();
		HBox root = new HBox();
		Scene scene = new Scene(root, 830, 600);
		Canvas c = new Canvas(600, 600);
		VBox menu = new VBox();
		menu.setPadding(new Insets(100, 0, 0, 0));
		menu.setSpacing(5);
		Button boutonReset = new Button("Rejouer");
		Button boutonDimension = new Button("Changer taille");
		HBox hbNom = new HBox();
		Label labelNom = new Label("Nom : ");
		tfNom = new TextField("Default");
		hbNom.getChildren().addAll(labelNom, tfNom);
		textTimer = new Text("Temps écoulé : 00:00");
		textScores = new Text("Scores :\n");
		afficherScores();
		ActionListener listener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Integer minutes;
				Integer secondes;
				secondesEcoulees++;
				minutes = secondesEcoulees/60;
				secondes = secondesEcoulees-minutes*60;
				DecimalFormat formater = new DecimalFormat("00");
				textTimer.setText("Temps écoulé : "+formater.format(minutes)+":"+formater.format(secondes));
			}
		};
		timer = new Timer(1000, listener);
		boutonReset.setPrefWidth(150);
		boutonDimension.setPrefWidth(150);
		gc = c.getGraphicsContext2D();
		gc.setLineWidth(1.2);
		gc.strokeRect(50, 50, 500, 500);
		textNbBombes = new Text("Nombre de bombes restantes: ");
		grille = Grille.getInstance();
		grille.setTailleGrille(intDim);
		//afficherScores();
		//Dessin des lignes et colonnes
		for(int i=0; i<intDim; i++) {
			gc.strokeLine(50, 50+i*(500/intDim), 550, 50+i*(500/intDim));
			gc.strokeLine(50+i*(500/intDim), 50, 50+i*(500/intDim), 550);
		}
		tfNom.setOnKeyReleased(e->{
			if(tfNom.getText().length() > 15)
				tfNom.setText(tfNom.getText().substring(0, tfNom.getText().length()-1));
		});
		c.setOnMouseClicked(e->{
			actionClick(intDim, e);
		});
		boutonReset.setOnAction(e->{
			reset(intDim);
		});
		boutonDimension.setOnAction(e->{
			choixDimension();
			reset(intDim);
		});
		menu.getChildren().addAll(textNbBombes, boutonReset, boutonDimension, textTimer, hbNom, textScores);
		root.getChildren().addAll(c, menu);
		stage.setScene(scene);
		stage.setTitle("Démineur");
		stage.setResizable(false);
		stage.show();
	}
}
