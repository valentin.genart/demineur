package main;

/**
 * Représente une case de la grille.
 * @author Valentin
 *
 */
public class Case {
	
	/**
	 * L'index i de la case dans la grille.
	 */
	private int i;
	
	/**
	 * Définit si la case contient une bombe ou non.
	 */
	private boolean isBomb;
	
	/**
	 * Définit si la case a été révélée par le joueur.
	 */
	private boolean isHidden;
	
	/**
	 * Définit si la case a été marquée par un flag par le joueur.
	 */
	private boolean isMarked;
	
	/**
	 * L'index j de la case dans la grille.
	 */
	private int j;
	
	/**
	 * La coordonnée X du GraphicsContext du milieu de la case (pour afficher le nombre au milieu de la case).
	 */
	private int milieuX;
	
	/**
	 * La coordonnée Y du GraphicsContext du milieu de la case (pour afficher le nombre au milieu de la case).
	 */
	private int milieuY;
	
	/**
	 * Le nombre de bombes dans les cases avoisinant la case.
	 */
	private int nbBombesAutour;
	
	/**
	 * Définit si la stack est déjà passée par cette case (utile pour dévoiler directement toute une zone de cases à 0).
	 */
	private boolean passageStack;

	/**
	 * Constructeur de la case.
	 * @param i l'index i de la case dans la grille.
	 * @param j l'index j de la case dans la grille.
	 * @param intDim la taille d'une dimension de la grille (utile pour calculer le milieu de la case).
	 */
	public Case(int i, int j, int intDim) {
		super();
		isHidden = true;
		isMarked = false;
		passageStack = false;
		this.i = i;
		this.j = j;
		milieuX = (50+(j+1)*500/intDim)-((50+(j+1)*500/intDim)-(50+j*(500/intDim)))/2;
		milieuY = (50+(i+1)*500/intDim)-((50+(i+1)*500/intDim)-(50+i*(500/intDim)))/2;
	}

	/**
	 * Retourne l'index i de la case dans la grille.
	 * @return l'index i de la case dans la grille.
	 */
	public int getI() {
		return i;
	}

	/**
	 * Retourne l'index j de la case dans la grille.
	 * @return l'index j de la case dans la grille.
	 */
	public int getJ() {
		return j;
	}
	
	/**
	 * Retourne la coordonnée x du milieu de la case dans le GraphicsContext.
	 * @return la coordonnée x du milieu de la case dans le GraphicsContext.
	 */
	public int getMilieuX() {
		return milieuX;
	}

	/**
	 * Retourne la coordonnée y du milieu de la case dans le GraphicsContext.
	 * @return la coordonnée y du milieu de la case dans le GraphicsContext.
	 */
	public int getMilieuY() {
		return milieuY;
	}
	
	/**
	 * Retourne le nombre de bombes dans le voisinnage de la case.
	 * @return le nombre de bombes dans le voisinnage de la case.
	 */
	public int getNbBombesAutour() {
		return nbBombesAutour;
	}

	/**
	 * Retourne true si la stack est déjà passée par cette case.
	 * @return true si la stack est déjà passée par cette case.
	 */
	public boolean getPassageStack() {
		return passageStack;
	}

	/**
	 * Retourne true si la case contient une bombe.
	 * @return true si la case contient une bombe.
	 */
	public boolean isBomb() {
		return isBomb;
	}

	/**
	 * Retourne false si la case a été révélée par le joueur.
	 * @return false si la case a été révélée par le joueur.
	 */
	public boolean isHidden() {
		return isHidden;
	}
	
	/**
	 * Retourne true si la case a été marquée par un flag par le joueur.
	 * @return true si la case a été marquée par un flag par le joueur.
	 */
	public boolean isMarked() {
		return isMarked;
	}

	/**
	 * Définit si la case contient une bombe ou non.
	 * @param b true si la case contient une bombe.
	 */
	public void setBomb(boolean b) {
		this.isBomb = b;
	}

	/**
	 * D�finit si la case a été révélée par le joueur.
	 * @param b false si la case a été révélée par le joueur.
	 */
	public void setHidden(boolean b) {
		this.isHidden = b;		
	}

	/**
	 * Définit si la case a été marquée par un flag par le joueur.
	 * @param isMarked true si la case a été marquée par un flag par le joueur.
	 */
	public void setMarked(boolean isMarked) {
		this.isMarked = isMarked;
	}
	
	/**
	 * Définit le nombre de bombes dans le voisinnage de la case.
	 * @param nbBombesAutour le nombre de bombes dans le voisinnage de la case.
	 */
	public void setNbBombesAutour(int nbBombesAutour) {
		this.nbBombesAutour = nbBombesAutour;
	}

	/**
	 * Définit si la stack est déjà passée par cette case.
	 * @param passageStack true si la stack est passée par cette case.
	 */
	public void setPassageStack(boolean passageStack) {
		this.passageStack = passageStack;
	}
}
