package main;

import java.util.Iterator;
import java.util.Random;

/**
 * La grille de cases, plateau de jeu
 * @author Valentin
 *
 */
public class Grille implements Iterable<Case>{
	
	/**
	 * Classe définissant un itérateur de cases sur une grille.
	 * @author Valentin
	 *
	 */
	private class GrilleIterator implements Iterator<Case>{

		/**
		 * Le compteur de cases de la grille.
		 */
		private int cpt;

		/**
		 * La Grille du jeu.
		 */
		private Grille grille;

		/**
		 * Constructeur de l'itérateur.
		 * @param grille la Grille du jeu
		 */
		public GrilleIterator(Grille grille) {
			super();
			this.grille = grille;
			this.cpt = -1;
		}

		/**
		 * Retourne true si il y a encore des éléments non parcourus dans la grille.
		 */
		@Override
		public boolean hasNext() {
			if(cpt<Math.pow(grille.getGrille().length, 2)-1)
				return true;
			return false;				
		}

		/**
		 * Retourne l'élément suivant dans la grille.
		 */
		@Override
		public Case next() {
			if(hasNext()) {
				cpt++;
				return grille.getGrille()[cpt/grille.getGrille().length][cpt-cpt/grille.getGrille().length*grille.getGrille().length];
			}
			return null;
		}
	}
	
	/**
	 * L'instance de la classe Grille
	 */
	private final static Grille instgrille = new Grille();

	/**
	 * Retourne la seule instance de la Grille.
	 * @return l'instance de la Grille.
	 */
	public static Grille getInstance() {
		return instgrille;
	}

	/**
	 * La structure représentant la grille.
	 */
	private Case[][] grille;

	/**
	 * Le nombre de bombes total de la grille.
	 */
	private int nbBombes;;
	
	/**
	 * Constructeur privé et vide (Singleton).
	 */
	private Grille() {}
	
	/**
	 * Compte le nombre de bombes dans les cases voisines de la case passée en paramètre.
	 * @param c la case dont on veut le nombre de bombes autour.
	 * @return le nombre de bombes autour de la case.
	 */
	private int compterBombesAutour(Case c) {
		int nbBombesAutour = 0, i, j;
		i = c.getI();
		j = c.getJ();
		for(int k=i-1; k<=i+1; k++) {
			for(int l=j-1; l<=j+1; l++) {
				if(k>=0 && l >=0 && k<grille.length && l<grille.length) {
					if(grille[k][l].isBomb())
						nbBombesAutour++;
				}	
			}
		}
		return nbBombesAutour;
	}

	/**
	 * Génère les bombes dans la grille. La case d'index initX et initY n'aura forcément pas de bombes et n'aura aucune bombe 
	 * dans son voisinnage, comme c'est la première case sur laquelle le joueur clique.
	 * @param initX l'index de 1ere dimension de la première case sur laquelle le joueur clique dans la grille, 
	 * afin d'initialiser cette case à 0.
	 * @param initY l'index de 2e dimension de la première case sur laquelle le joueur clique dans la grille, 
	 * afin d'initialiser cette case à 0.
	 */
	private void generateBombs(int initX, int initY) {
		Iterator<Case> it = iterator();
		Case c;
		int i, j;
		while(it.hasNext()) {
			c = it.next();
			i = c.getI();
			j = c.getJ();
			if(i >= initX-1 && i <= initX+1 && j >= initY-1 && j <= initY+1)
				c.setBomb(false);
			else {
				Random r = new Random();
				if(r.nextInt(100)+1 < 25) {
					c.setBomb(true);
					nbBombes++;
				}
				else
					c.setBomb(false);
			}
		}
	}

	/**
	 * Génère les bombes ainsi que le nombre de bombes avoisinant chaque case.
	 * @param initX l'index de 1ere dimension de la première case sur laquelle le joueur clique dans la grille, 
	 * afin d'initialiser cette case à 0.
	 * @param initY l'index de 2e dimension de la première case sur laquelle le joueur clique dans la grille, 
	 * afin d'initialiser cette case à 0.
	 */
	public void generateGrille(int initX, int initY) {
		for(int i=0; i<grille.length; i++) {
			for(int j=0; j<grille.length; j++)
				grille[i][j] = new Case(i, j, grille.length);
		}
		generateBombs(initX, initY);
		generateNumbers();
	}

	/**
	 * Génère, pour chaque case qui ne contient pas de bombe, le nombre de bombes qu'il y a dans les cases voisines.
	 */
	private void generateNumbers() {
		Iterator<Case> it = iterator();
		Case c;
		while(it.hasNext()) {
			c = it.next();
			if(!c.isBomb())
				c.setNbBombesAutour(compterBombesAutour(c));
		}
	}

	/**
	 * Retourne la grille
	 * @return la grille
	 */
	public Case[][] getGrille(){
		return grille;
	}

	/**
	 * Retourne le nombre total de bombes dans la grille
	 * @return le nombre total de bombes dans la grille
	 */
	public int getNbBombes() {
		return nbBombes;
	}

	/**
	 * Retourne la taille d'une dimension de la grille.
	 * @return la taille d'une dimension de la grille.
	 */
	public int getTailleGrille() {
		return grille.length;
	}

	/**
	 * Retourne un itérateur de cases sur la grille.
	 */
	@Override
	public Iterator<Case> iterator() {
		return new GrilleIterator(this);
	}
	
	/**
	 * Affiche la grille complètement révélée dans la console.
	 */
	public void printGrille() {
		String print="";
		Iterator<Case> it = iterator();
		Case c;
		int i, j;
		while(it.hasNext()) {
			c = it.next();
			i = c.getI();
			j = c.getJ();
			if(grille[i][j].isBomb())
				print+="X ";
			else
				print+=grille[i][j].getNbBombesAutour()+" ";
			if(j == grille.length-1)
				print+="\n";
		}
		System.out.println(print);
	}
	
	/**
	 * Constructeur de la grille.
	 * @param i longueur et largeur de la grille.
	 */
	public void setTailleGrille(int i) {
		nbBombes = 0;
		this.grille = new Case[i][i];
	}
}
