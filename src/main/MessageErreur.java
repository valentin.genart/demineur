package main;

/**
 * Exception levée lors de n'importe quelle erreur affichant un message d'erreur.
 * @author Valentin
 *
 */
public class MessageErreur extends Exception {
	
	private static final long serialVersionUID = 1L;
	
	/**
	 * Le message d'erreur.
	 */
	private String message;

	/**
	 * Constructeur créant le message d'erreur.
	 */
	public MessageErreur() {
		super();
		message = "Une erreur est survenue.";
	}

	/**
	 * Renvoie le message d'erreur.
	 */
	public String getMessage() {
		return message;
	}
}
